let swiper = new Swiper(".swiper", {
    loop: true,
    slidesPerView: 3,
    effect: "coverflow",
    coverflowEffect: {
        rotate: 20,
        slideShadows: true
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
    },
    keyboard: {
        enabled: true,
        onlyInViewport: false
    }
})